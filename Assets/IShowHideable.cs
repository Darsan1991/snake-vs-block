﻿using System;

public interface IShowHideable
{
    bool showing { get; }
    void Show(bool animate = true, Action OnFinished = null);
    void Hide(bool animate = true, Action OnFinished = null);
}