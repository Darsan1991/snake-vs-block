﻿using UnityEngine;

public class AudioPlayer : IAudioController
{
    private readonly AudioSource audioSource;

    public void Play() => audioSource.Play();

    public void Stop() => audioSource.Stop();

    public float volume { get { return audioSource.volume; } set { audioSource.volume = value; } }
    public bool enable { get { return audioSource.enabled; } set { audioSource.enabled = value; }}

    public AudioPlayer(AudioSource audioSource)
    {
        this.audioSource = audioSource;
    }
}