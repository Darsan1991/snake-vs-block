﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverPanel:MonoBehaviour,IInitializable<GameOverPanel.InitParams>
{
    private static int EXIT_HASH = Animator.StringToHash("Exit");
    [SerializeField] private Text scoreTxt, bestTxt;
    [SerializeField] private Button soundBtn;
    [SerializeField] private Sprite[] soundOnAndOffSprites;
    [SerializeField] private Animator anim;

    public State currentState { get; private set; }

    public bool inilized { get; private set; }

    public bool hasSound => PrefManager.IsSound();

    public void Init(InitParams t)
    {
        var languageWordForWord = ResourceManager.GetLanguageWordForWord("Your Score")??"Your Score";
        scoreTxt.text = languageWordForWord+" : "+t.score;
        var wordForWord = ResourceManager.GetLanguageWordForWord("Best Score")??"Best Score";
        bestTxt.text = wordForWord+" : "+ t.bestScore;
        inilized = true;
    }

    public void Activate()
    {
        RefreshUis();
        gameObject.SetActive(true);
    }

    private void RefreshUis()
    {
        soundBtn.image.sprite = soundOnAndOffSprites[hasSound ? 0 : 1];
    }

    public void OnClickRatingButton()
    {
        GeneralButtons.RatingButton();
    }

    public void OnClickShareButton()
    {
        GeneralButtons.ShareButton();
    }

    public void OnClickTopScoreButton()
    {
        GeneralButtons.TopScoreButton();
    }

    public void OnClickSoundButton()
    {
        PrefManager.SetSound(!hasSound);
        RefreshUis();
    }

    public void OnClickHomeButton()
    {
        ExitAndGO(State.GOING_TO_HOME);
    }

    public void OnClickReplay()
    {
        ExitAndGO(State.REPLAYING);
    }

    void ExitAndGO(State targetState)
    {
        currentState = targetState;
        anim.SetTrigger(EXIT_HASH);
    }

    public void OnExitAnimationFinished()
    {
        switch (currentState)
        {
             case State.GOING_TO_HOME:
                GameManager.LoadGame();
                break;

             case State.REPLAYING:
                GameManager.LoadGame(GameStartState.Restart);
                break;
        }
        
    }

   
    

    #region Inner Class,Enums,Structs

    public struct InitParams
    {
        public int score { get; set; }
        public int bestScore { get; set; }
    }

    public enum State
    {
        NONE,GOING_TO_HOME,REPLAYING
    }

    #endregion
}