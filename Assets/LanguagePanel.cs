using System.Collections.Generic;
using UnityEngine;

public class LanguagePanel : RadioGroup
{

    [SerializeField] private LanguageTileUI languageTileUiPrefab;
    [SerializeField] private RectTransform contentTransform;
    public override IRadioGroupTile[] radioGroupTiles { get; protected set; }

    public override void Init()
    {
        var languageAndNames =  
//            new List<LanguageAndName>
//        {
//            new LanguageAndName{language = Language.English,name = "English"},
//            new LanguageAndName{language = Language.Hindi,name = "Hindi"}
//        }; 

       ResourceManager.languagesAndNames;
        var radioGroups = new List<IRadioGroupTile>();
        if (languageAndNames != null)
        {
            foreach (var languageAndName in languageAndNames)
            {
                var languageTileUi = Instantiate(languageTileUiPrefab);
                languageTileUi.languageAndName = languageAndName;
                languageTileUi.transform.parent = contentTransform;
                languageTileUi.transform.localScale = Vector3.one;
                radioGroups.Add(languageTileUi);
            }
        }
       
        radioGroupTiles = radioGroups.ToArray();
        base.Init();
        SetAsSelected(radioGroups.Find((t)=>((LanguageTileUI)t).languageAndName.language == PrefManager.GetCurrentLanguage()));
    }

    protected override void Start()
    {
        base.Start();
        PrefManager.SetKey(PrefManager.SETTLED_LANGUAGE_KEY);
    }

    public void OnSelectComplete()
    {
        if (selected!=null)
        {
            PrefManager.SetCurrentLanguage(((LanguageTileUI)radioGroupTiles[(int)selected]).languageAndName.language);
            
        }
        gameObject.SetActive(false);
    }
}