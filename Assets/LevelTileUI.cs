﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class LevelTileUI : MonoBehaviour,IInitializable<LevelTileUI.InitParams>
{
    public static event Action<LevelTileUI> OnLevelTileUIClicked;

    [SerializeField] private Text levelNoTxt;
    [SerializeField] private Color[] lockedAndUnlockColor;
    [SerializeField] private Image bgImage;

    public bool inilized { get; private set; }
    public int levelNo { get; private set; }
    public bool locked
    {
        get
        {
            return _locked;
        }
        set
        {
            _locked = value;
//            button.interactable = !locked;
            bgImage.color = locked ? lockedAndUnlockColor[0] : lockedAndUnlockColor[1];
        }
    }

    private Button button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }


    private bool _locked;
    private Button _button;
    


    public void Init(InitParams t)
    {
        if(inilized)
            return;
        levelNoTxt.text = t.levelNo.ToString();
        levelNo = t.levelNo;
        locked = t.locked;
        inilized = true;

    }

    public void OnClickButton() => OnLevelTileUIClicked?.Invoke(this);


    public struct InitParams
    {
        public int levelNo;
        public bool locked;
    }

   
}