﻿using UnityEngine;

public abstract class RadioGroup : MonoBehaviour,IInitializable
{
    public int? selected { get; private set; }

    public abstract IRadioGroupTile[] radioGroupTiles { get; protected set; }
    public bool inilized { get; private set; }


    protected virtual void Start()
    {
        LateCall.Create().Call(()=>ResourceManager.Inilized,Init);
    }

    protected void SetAsSelected(IRadioGroupTile tile)
    {
        var i = GetTheIndexOfTile(tile);
        if (i == selected || i<0)
            return;
        if (selected != null)
        {
            radioGroupTiles[(int)selected].selected = false;
        }
        tile.selected = true;
        selected = i;
    }
    private void OnRadioGroupTileClicked(IRadioGroupTile tile)
    {
        
        SetAsSelected(tile);
    }

    public int GetTheIndexOfTile(IRadioGroupTile tile)
    {
        var groupTiles = radioGroupTiles;
        for (var i = 0; i < groupTiles.Length; i++)
        {
            if (groupTiles[i] == tile)
            {
                return i;
            }
        }

        return -1;
    }

    public virtual void Init()
    {
        if (inilized)
        {
            return;
        }

        foreach (var radioGroupTile in radioGroupTiles)
        {
            radioGroupTile.OnClick += OnRadioGroupTileClicked;
        }

        inilized = true;
    }
}