﻿using System;
using UnityEngine;

public class LevelPanel : MonoBehaviour,IInitializable,IShowHideable
{
    private static readonly int SHOW_HASH = Animator.StringToHash("Show");
    [SerializeField] private LevelTileUI levelTileUIPrefab;
    [SerializeField] private RectTransform contentPanel;
    [SerializeField] private Animator anim;

    public bool inilized { get; private set; }
    public bool showing { get { return gameObject.activeSelf; } private set{gameObject.SetActive(value);}}

    private void Awake()
    {
        Init();
    }

   public  void Init()
    {
        if(inilized)
            return;
        var levels = GameManager.GetAllLevels();
        foreach (var level in levels)
        {
            var levelTileUi = Instantiate(levelTileUIPrefab);
            levelTileUi.transform.parent = contentPanel;
            levelTileUi.transform.localScale = Vector3.one;
            levelTileUi.Init(new LevelTileUI.InitParams
            {
                levelNo = level.levelNo,
                locked = level.locked
            });
        }
        inilized = true;

    }

    public void Show(bool animate = true, Action OnFinished = null)
    {
        if(showing)
            return;
        showing = true;
        if (animate)
        {
            anim.SetBool(SHOW_HASH, true);
            var currentTime = Time.time;
            LateCall.Create().Call(() => currentTime + 0.5f < Time.time,OnFinished);
        }
        else
        {
            OnFinished?.Invoke();
        }
    }

    public void OnClickBack()
    {
        Hide();
    }

    public void Hide(bool animate = true, Action OnFinished = null)
    {
        if(!showing)
            return;
        if (animate)
        {
        anim.SetBool(SHOW_HASH,false);
            var currentTime = Time.time;
            LateCall.Create().Call(()=>currentTime+0.5f<Time.time, () =>
            {
                showing = false;
                OnFinished?.Invoke();
            });
        }
        else
        {
            showing = false;
            OnFinished?.Invoke();
            
        }
    }
}