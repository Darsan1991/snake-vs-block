﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour,IDamageable
{
    public event Action<IReward> OnGotReward; 
    public event Action<int> OnPartCountChanged;
    public event Action OnPlayerDie; 

    public const float PART_RADIUS = 0.218f * 0.7f;


    [SerializeField] private int maxVisiblePartCount;
    [SerializeField] private PlayerTilePart playerTilePrefab;
    [SerializeField] private PlayerTilePart playerHeadPrefab;
    [SerializeField] private float speed;
    [SerializeField] private float headOffsetResumeLerb;
    [SerializeField] private GameObject dieParticleEffectPrefab;
    [SerializeField] private AudioSource audioSource;


    public int value =>partCount + (playerHead!=null?1:0);

    private readonly Path path = new Path();
    private readonly List<PlayerTilePart> playerTilePartList = new List<PlayerTilePart>();

    public State currentState
    {
        get
        {
            return _currentState;
        }
        private set
        {
            if (_currentState != value)
            {
                var lastState = _currentState;
                _currentState = value;
                OnCurrentStateChanged(lastState, currentState);
            }
        }
    }


    public int partCount
    {
        get { return _partCount; }
        private set
        {
            if (_partCount!=value)
            {
                _partCount = value;
                OnPartCountChanged?.Invoke(partCount);
            }
        }
    }

    public Rigidbody2D mRigid
    {
        get
        {
            if (!_mRigid)
            {
                _mRigid = GetComponent<Rigidbody2D>();
            }
            return _mRigid;
        }
    }

    private PlayerTilePart playerHead;
    private int maxStreamCount = 500;
    private Rigidbody2D _mRigid;
    private float xPos;
    private float headOffset=0f;
    private State _currentState = State.NONE;
    private int _partCount;
    private IAudioController audioController;

    private void Awake()
    {
        audioController = new AudioPlayer(audioSource);
        playerHead = Instantiate(playerHeadPrefab,transform.position,Quaternion.identity);
        AddPlayerParts(4);
    }

    public void Damage(int count=1)
    {
        if(currentState !=State.PLAYING)
            return;

        while (partCount>0&&count>0)
        {
            DestroyPart();
            count--;
        }

        if(count>0)
            Die();
    }

    public void StartPlay()
    {
        currentState = State.PLAYING;
        AudioManager.PlayIfSound(audioController);
    }

    public void StopPlay()
    {
        currentState = State.NONE;
        AudioManager.StopIfSound(audioController);

    }

    private void Update()
    {
       
        if (Input.GetKeyDown(KeyCode.I))
            AddPlayerParts(5);

        if (Input.GetKeyDown(KeyCode.T))
        {
            DestroyPart();
        }
    }

    private void Die()
    {
        if(currentState != State.PLAYING)
            throw new Exception("Player is not in playing state");
        AudioManager.StopIfSound(audioController);
        currentState = State.DIE;
        Destroy(playerHead.gameObject);
        playerHead = null;
        OnPlayerDie?.Invoke();

    }

    public void DestroyPart()
    {
        partCount--;

        var tilePart = playerTilePartList[0];
        playerTilePartList.RemoveAt(0);
        var system = Instantiate(dieParticleEffectPrefab, playerHead.transform.position, Quaternion.identity);
        Destroy(system.gameObject, 3f);
        Destroy(tilePart.gameObject);
        headOffset = PART_RADIUS * 2;


        if (partCount >= maxVisiblePartCount)
        {
            CreateTileParts(maxVisiblePartCount - playerTilePartList.Count);
        }
    }

    public void AddPlayerParts(int count)
    {
        partCount += count;
        if (playerTilePartList.Count + count <= maxVisiblePartCount)
        {
            CreateTileParts(count);
        }
        else
        {
            CreateTileParts(maxVisiblePartCount - playerTilePartList.Count);
        }
    }


    private void OnCurrentStateChanged(State lastState, State state)
    {
        if (lastState == State.PLAYING)
        {
            InputController.OnTouchDrag -= OnTouchDrag;
        }
        if (state == State.PLAYING)
        {
            InputController.OnTouchDrag += OnTouchDrag;
        }
    }

    private void OnTouchDrag(Vector2 delta)
    {
        xPos = xPos + delta.x * 3f;
    }

    private void FixedUpdate()
    {
        if (currentState == State.PLAYING)
        {
            #region Movement

            mRigid.velocity = Vector2.zero;
            float lerb = 0.05f;
//            var difference = Mathf.Abs(xPos - mRigid.position.x) - 1f;
//            if (difference <= 0)
//                lerb = 0.1f;
            var x = Mathf.Lerp(mRigid.position.x, xPos, lerb);
            mRigid.MovePosition((new Vector2(x
                , 1 * speed * Time.deltaTime + mRigid.position.y)));
            mRigid.velocity = Vector2.zero;

            #endregion

            if (headOffset>0)
            {
                headOffset = Mathf.Max(headOffset - headOffsetResumeLerb * Time.deltaTime, 0);
            }

            if (path.Count > 0)
            {
                if ((path[0] - (Vector2)transform.position).sqrMagnitude > 0)
                {
                    path.Insert(0, transform.position);
                }
            }
            else
            {
                path.Insert(0, transform.position);
            }

            while (path.Count > maxStreamCount)
            {
                path.RemoveAt(path.Count - 1);
            }

            var segmentPoints = path.FindSegmentPoints(2 * PART_RADIUS, playerTilePartList.Count + 1,headOffset,2.05f*PART_RADIUS);
            if (segmentPoints != null)
            {
                
                    playerHead.SetPosition(segmentPoints.Length>0?segmentPoints[0]:path[0]);
                for (var i = 1; i < segmentPoints.Length; i++)
                {
                    playerTilePartList[i - 1].SetPosition(segmentPoints[i]);
                }
            }
           

           

        }
           
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        foreach (var contactPoint2D in other.contacts)
        {
            if (Mathf.Abs(Vector2.Dot(contactPoint2D.normal.normalized, Vector2.right)) > 0.5f)
            {
                xPos = mRigid.position.x;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Rewarder"))
        {
            var reward = other.GetComponent<IRewardable>().GetReward();
            var pointsReward = reward as IPointsReward;
            if(pointsReward != null)
                AddPlayerParts(pointsReward.points);

            OnGotReward?.Invoke(reward);
        }
    }

    private void CreateTileParts(int count)
    {
        var targetPos = playerTilePartList.Count > 0
            ? playerTilePartList[playerTilePartList.Count - 1].transform.position
            : transform.position;
//        print("target Pos:"+targetPos);
        for (int i = 0; i < count; i++)
        {
            var playerTilePart = Instantiate(playerTilePrefab, targetPos, Quaternion.identity);
            playerTilePart.gameObject.name = "circle_" + i;
            playerTilePartList.Add(playerTilePart);
        }
    }

    public enum State
    {
        NONE,PLAYING,DIE
    }
}

public interface IDamageable
{
    int value { get; }
    void Damage(int count=1);
}