﻿using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance { get; private set; }
    public static AudioClip ON_CLICK_CLIP => instance.clickClip;
    [SerializeField] private AudioClip gotPointsClip, losePointsClip, gameOverClip,levelCompleteClip, clickClip;

    public static bool isSound => PrefManager.IsSound();

    private readonly List<IAudioController> audioControllers= new List<IAudioController>();

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        Block.OnTookDamage += OnTookDamage;
        LevelManager.OnGameStart += OnGameStart;
        LevelManager.OnGameOver += OnGameOver;
        LevelManager.OnLevelComplete += OnLevelComplete;

    }



    private void OnDisable()
    {
        Block.OnTookDamage -= OnTookDamage;
        LevelManager.OnGameStart -= OnGameStart;
        LevelManager.OnGameOver -= OnGameOver;
        LevelManager.OnLevelComplete -= OnLevelComplete;
    }

    private void OnLevelComplete(int lvl)
    {
        if (isSound)
            AudioSource.PlayClipAtPoint(levelCompleteClip, Camera.main.transform.position);
    }

    private void OnGameOver()
    {
        LevelManager.instance.player.OnGotReward -= OnPlayerGotReward;
        if(isSound)
        AudioSource.PlayClipAtPoint(gameOverClip,Camera.main.transform.position);
    }

    public static void PlayIfSound(IAudioController audioController)
    {
        if(!isSound)
            return;
        var controllers = instance.audioControllers;
        if (!controllers.Contains(audioController))
        {
            controllers.Add(audioController);
        }
        audioController.Play();
    }

    public static void StopIfSound(IAudioController audioController)
    {
        if(!isSound)
        return;
        var controllers = instance.audioControllers;
        if (controllers.Contains(audioController))
        {
            controllers.Remove(audioController);
            audioController.Stop();
        }
    }

    private void OnGameStart()
    {
        LevelManager.instance.player.OnGotReward += OnPlayerGotReward;
    }

    private void OnPlayerGotReward(IReward obj)
    {
        if (isSound)
        {
        AudioSource.PlayClipAtPoint(gotPointsClip,Camera.main.transform.position);
        }
    }


    private void OnTookDamage(Block block, int points)
    {
        if(isSound)
        AudioSource.PlayClipAtPoint(losePointsClip,Camera.main.transform.position);
    }

    public static void Play(AudioClip clip)
    {
        if (isSound)
        {
        AudioSource.PlayClipAtPoint(clip,Camera.main.transform.position);
        }
    }
}