﻿using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;

public class MenuPanel : MonoBehaviour
{
    private static int PLAY_HASH = Animator.StringToHash("Play");
    [SerializeField] private Animator anim;
    [SerializeField] private Button soundBtn;
    [SerializeField] private Sprite[] soundOnAndOffSprites;
    [SerializeField] private Text bestScoreTxt;


    private LevelManager levelManager;

    public bool showing { get; private set; }
    private bool hasSound => PrefManager.IsSound();

    private void Awake()
    {
        bestScoreTxt.text = PrefManager.GetBestScore().ToString();
    }

    private void Start()
    {
        levelManager = LevelManager.instance;
    }

    public void Show(bool animate=true)
    {
        showing = true;
        RefreshUis();
        gameObject.SetActive(true);

    }

    public void OnClickRatingButton()
    {
        GeneralButtons.RatingButton();
    }

    public void OnClickShareButton()
    {
        GeneralButtons.ShareButton();
    }

    public void OnClickTopScoreButton()
    {
        GeneralButtons.TopScoreButton();
    }

    public void OnClickSoundButton()
    {
        PrefManager.SetSound(!hasSound);
        RefreshUis();
    }

    public void OnClickLevelButton()
    {
        UIManager.instance.levelPanel.Show();
    }

    private void RefreshUis()
    {
        soundBtn.GetComponentInChildren<Image>().sprite = hasSound ? soundOnAndOffSprites[0] : 
            soundOnAndOffSprites[1];
    }

    public void Hide(bool animate=true)
    {
        showing = false;
        if(animate)
            anim.SetTrigger(PLAY_HASH);
        else
        {
            gameObject.SetActive(false);
        }

    }

    public void OnClickPlay()
    {
        Hide();
        levelManager.StartTheGame();
    }

    public void OnAnimationFinished()
    {
        gameObject.SetActive(false);  
        
    }
}

public enum GameStartState
{
    New,Restart,Continues
}