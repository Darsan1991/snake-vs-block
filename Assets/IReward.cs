public interface IRewardable
{
    /// <summary>
    /// Call To Get Reward
    /// </summary>
    IReward GetReward();
}

public interface IReward
{
    
}

public interface IPointsReward : IReward
{
    /// <summary>
    /// Points
    /// </summary>
    int points { get; }
}