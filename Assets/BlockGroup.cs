using UnityEngine;

public class BlockGroup : MonoBehaviour,ICycleTile
{
    [SerializeField] private Transform startPoint;
    [SerializeField] private Transform endPoint;

    public float height => (endPoint.position - startPoint.position).magnitude;
    public Vector2 position => startPoint.position;


    public void SetPosition(Vector2 position)
    {
        transform.position = (transform.position - startPoint.position) + (Vector3) position;
    }
}