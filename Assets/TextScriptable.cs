using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Text Scriptable", menuName = "Scriptable/Text Scriptable")]
public class TextScriptable : ScriptableObject
{
    [SerializeField] private List<WordLanguageGroup> _wordAndLanguageGroups;
    public IReadOnlyCollection<WordLanguageGroup> wordLanguageGroups => _wordAndLanguageGroups.AsReadOnly();

}

[System.Serializable]
public struct LanguageAndName
{
    public Language language;
    public string name;
}

[System.Serializable]
public struct WordLanguageGroup
{
    public string word;
    public List<LanguageAndWord> languagesAndWords;

    public string GetWordForLanguage(Language language)
    {
        if (languagesAndWords==null)
        {
            return "";
        }
        int index = languagesAndWords.FindIndex((l) => l.language == language);

        return index==-1?GetWordForLanguage(Language.English):languagesAndWords[index].word;
    }
    
}

[System.Serializable]
public struct LanguageAndWord
{
    public Language language;
    public string word;
}

public enum Word
{
    Best,Score
}

public enum Language
{
    English,Hindi
}

