﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayPanel : MonoBehaviour
{
    [SerializeField] private Text scoreTxt;
    [SerializeField] private Text levelTxt;

    private int score
    {
        get
        {
            return _score;
        }
        set
        {
            _score = value;
            if(scoreTxt)
            scoreTxt.text = _score.ToString();
        }
    }

    private int _score;

    private LevelManager levelManager;


    private void OnEnable()
    {

        levelManager = LevelManager.instance;
        RefreshUis();
        LevelManager.OnScoreChanged += OnScoreChanged;
        if (LevelManager.instance.loadedGameDetails.gameStartState == GameStartState.Continues)
        {
            Invoke(nameof(ShowLevelTxt),0.5f);
            Invoke(nameof(HideLevelTxt),2.5f);
        }

    }

    void ShowLevelTxt()
    {
        levelTxt.text = (ResourceManager.GetLanguageWordForWord("Level")??"Level") +" "+levelManager.level;
        levelTxt.gameObject.SetActive(true);
        StartCoroutine(ShowHideLevel(true));
    }

    void HideLevelTxt()
    {
        StartCoroutine(ShowHideLevel(false,()=>levelTxt.gameObject.SetActive(false)));
    }

    public void Pause()
    {
        if (Time.timeScale > 0)
            Time.timeScale = 0f;
    }

    public void Resume()
    {
        if (Time.timeScale<=0)
        {
            Time.timeScale = 1f;
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

    private void OnScoreChanged(int score)
    {
        this.score = score;
    }

    void RefreshUis()
    {
        score = levelManager.score;
    }

    IEnumerator ShowHideLevel(bool show,Action OnFinished=null)
    {
        var target = show ? 1f : 0;
        while (Mathf.Abs(target-levelTxt.color.a)>0.01f)
        {
            var lerp = Mathf.Lerp(levelTxt.color.a,target,5f*Time.deltaTime);
            var levelTxtColor = levelTxt.color;
            levelTxtColor.a = lerp;
            levelTxt.color = levelTxtColor;
            yield return null;
        }
        OnFinished?.Invoke();
    }

    private void OnDisable()
    {
        
    }
}