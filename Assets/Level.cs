﻿public class Level
{
    public readonly int levelNo;
    public  bool locked { get; private set; }

    public Level(int levelNo,bool locked=true)
    {
        this.levelNo = levelNo;
        this.locked = locked;
    }

    public void UnlockTheLevel()
    {
        locked = false;
    }
}