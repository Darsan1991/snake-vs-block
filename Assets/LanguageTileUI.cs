﻿using System;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Button))]
public class LanguageTileUI : RadioGroupTile
{
    public override event Action<IRadioGroupTile> OnClick;

    [SerializeField] private Text text;
    [SerializeField] private Toggle toggle;
    public override bool selected { get { return toggle.isOn; } set { toggle.isOn = value; } }


    public LanguageAndName languageAndName
    {
        get
        {
            return _languageAndName;
        }
        set
        {
            _languageAndName = value;
            text.text = languageAndName.name;
        }
    }


    private LanguageAndName _languageAndName;



    private void OnEnable()
    {
        var button = GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(OnClickButton);
    }

    private void OnClickButton()
    {
        OnClick?.Invoke(this);
    }
}

public abstract class RadioGroupTile : MonoBehaviour, IRadioGroupTile
{
    public abstract event Action<IRadioGroupTile> OnClick;
    public abstract bool selected { get; set; }
}

public interface IRadioGroupTile
{
    event Action<IRadioGroupTile> OnClick;
    bool selected { get; set; }
}