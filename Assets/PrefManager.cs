using System;
using UnityEngine;

public static class PrefManager
{
    public static event Action<int> OnBestScoreChanged; 
    public static event Action<Language> OnLanguageChanged; 
    private const string SCORE = "score";
    private const string SOUND = "sound";
    private const string LANGUAGE = "language";
    private const string LEVEL = "level";

    public const string SHOWED_TUTORIAL_KEY = "tutorial";
    public const string SETTLED_LANGUAGE_KEY = "setted_language";

    public static int GetBestScore()=> PlayerPrefs.GetInt(SCORE, 0);

    public static void SetBestScore(int score)
    { 
        if(score==GetBestScore())
            return;
        PlayerPrefs.SetInt(SCORE, score);
        OnBestScoreChanged?.Invoke(score);

    } 
    public static bool IsSound() => PlayerPrefs.GetInt(SOUND, 1) == 1;
    public static void SetSound(bool hasSound) => PlayerPrefs.SetInt(SOUND,hasSound?1:0);
    public static void SetKey(string key) => PlayerPrefs.SetInt(key, 1);
    public static bool HasKey(string key) => PlayerPrefs.HasKey(key);
    public static void RemoveKey(string key) => PlayerPrefs.DeleteKey(key);
    public static int GetCurrentLevel() => PlayerPrefs.GetInt(LEVEL, 1);
    public static void SetCurrentLevel(int level) => PlayerPrefs.SetInt(LEVEL, level);
    public static void SetCurrentLanguage(Language language)
    {
        if(GetCurrentLanguage()==language)return;
        PlayerPrefs.SetInt(LANGUAGE,(int)language);
        OnLanguageChanged?.Invoke(language);
    }

    public static Language GetCurrentLanguage() => (Language) PlayerPrefs.GetInt(LANGUAGE, 0);

}