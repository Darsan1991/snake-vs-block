﻿public interface IAudioController
{
    void Play();
    void Stop();
    float volume { get; set; }
    bool enable { get; set; }
}