﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmationPanel : MonoBehaviour
{

    [SerializeField] private Text titleTxt;
    [SerializeField] private Text messageTxt;
    [SerializeField] private Button positiveBtn;
    [SerializeField] private Button negativeBtn;


    public ViewModel viewModel
    {
        get
        {
            return _viewModel;
        }
        set
        {
            if (_viewModel.title!=value.title)
            {
                _viewModel = value;
                titleTxt.text = value.title;
                messageTxt.text = value.message;
                positiveBtn.GetComponentInChildren<Text>().text = value.positiveBtnTitle;
                negativeBtn.GetComponentInChildren<Text>().text = value.negativeBtnTitle;
            }
        }
    }

    public bool active { get { return gameObject.activeSelf; } set { gameObject.SetActive(value);} }

    private ViewModel _viewModel;



    public void OnClickPositive()
    {
        viewModel.OnResult?.Invoke(true);
        gameObject.SetActive(false);
    }

    public void OnClickNegative()
    {
        viewModel.OnResult?.Invoke(false);
        gameObject.SetActive(false);
    }

    public struct ViewModel
    {
        public string title;
        public string message;
        public string positiveBtnTitle;
        public string negativeBtnTitle;
        public Action<bool> OnResult;
    }
}
