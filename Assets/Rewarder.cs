using UnityEngine;

public abstract class Rewarder : MonoBehaviour,IRewardable
{
    protected virtual IReward reward { get;}
    public abstract IReward GetReward();
}