﻿using System;
using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;

public class PatTest
{
    public class FindSegmentPoints
    {
        [Test]
        public void Return_Null_When_Path_Empty()
        {
            var path = new Path();
            var findSegmentPoints = path.FindSegmentPoints(0.2f, 10);
            Assert.IsNull(findSegmentPoints);
        }

        [Test]
        public void Points_Cannot_Exceed_Max_Points()
        {
            var path = new Path()
            {
                new Vector2(0, 0),
                new Vector2(1, 2),
                new Vector2(3, 5),
                new Vector2(5, 8),
                new Vector2(10, 15),
            };
            var maxPointsCount = 10;
            var findSegmentPoints = path.FindSegmentPoints(0.2f, maxPointsCount);
            Assert.IsTrue(findSegmentPoints.Length<=maxPointsCount);
        }

        [Test]
        public void Throw_Exception_When_Negative_Or_0_Segment()
        {
            var path = new Path();

            Assert.Throws<ArgumentException>(()=>path.FindSegmentPoints(-1));
            
        }

        [Test]
        public void Segment_Length_Between_Points()
        {
            var path = new Path()
            {
                new Vector2(0, 0),
                new Vector2(1, 2),
                new Vector2(3, 5),
                new Vector2(5, 8),
                new Vector2(10, 15),
            };

            var findSegmentPoints = path.FindSegmentPoints(0.5f,3);
            Assert.AreEqual((findSegmentPoints[1]-findSegmentPoints[0]).magnitude,0.5f);
            Assert.AreEqual((findSegmentPoints[2]-findSegmentPoints[1]).magnitude
                , (findSegmentPoints[1] - findSegmentPoints[0]).magnitude
                );
        }

        [Test]
        public void Cannot_Be_Single_Point()
        {
            var path = new Path()
            {
                new Vector2(0, 0),
                new Vector2(1, 2),
                new Vector2(3, 5),
                new Vector2(5, 8),
                new Vector2(10, 15),
            };

            Assert.AreNotEqual(path.FindSegmentPoints(200).Length,1);
            
        }
        [Test]
        public void First_Point_Should_First_Path_Point()
        {
            var path = new Path()
            {
                new Vector2(0, 0),
                new Vector2(1, 2),
                new Vector2(3, 5),
                new Vector2(5, 8),
                new Vector2(10, 15),
            };
            var points = path.FindSegmentPoints(3);
            Assert.AreEqual(points[0],path[0]);
        }
    }
}