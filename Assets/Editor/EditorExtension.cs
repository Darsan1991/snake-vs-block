﻿using UnityEditor;
using UnityEngine;

public static class EditorExtension
{
    [MenuItem("My Games/PlayerPrefs/Delete All")]
    public static void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }
}