﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LevelCompletePanel : MonoBehaviour,IShowHideable
{
    private static readonly int SHOW_HASH = Animator.StringToHash("Show");

    [SerializeField] private Text scoreTxt, bonusTxt,levelTxt;
    [SerializeField] private Animator anim;

    public bool showing { get { return gameObject.activeSelf; } private set{gameObject.SetActive(value);} }

    public ViewModel viewModel
    {
        get
        {
            return _viewModel;
        }
        set
        {
            _viewModel = value;
            scoreTxt.text = viewModel.score.ToString();
            bonusTxt.text = viewModel.bonus.ToString();
            
            levelTxt.text = (ResourceManager.GetLanguageWordForWord("Level")??"Level")+" " + value.level + " "+(ResourceManager.GetLanguageWordForWord("Completed")??"Completed")+"!";
        }
    }

    private ViewModel _viewModel;


    public void Show(bool animate = true, Action OnFinished = null)
    {
        if(showing)
            return;
        showing = true;
        if (animate)
        {
            anim.SetBool(SHOW_HASH,true);
            var currentTime = Time.time;
            LateCall.Create().Call(()=>Time.time>currentTime+0.5f,OnFinished);
        }
        else
        {
            OnFinished?.Invoke();
        }

    }


    public void Hide(bool animate = true, Action OnFinished = null)
    {
        if(!showing)
            return;
        if (animate)
        {
            anim.SetBool(SHOW_HASH, false);
            var currentTime = Time.time;
            LateCall.Create().Call(() => Time.time > currentTime + 0.7f, () =>
            {
                showing = false;
                OnFinished?.Invoke();
            });

        }
        else
        {
            showing = false;
            OnFinished?.Invoke();
        }
    }

    public void OnClickNextLevelButton()
    {
        GameManager.LoadGame(GameStartState.Continues,LevelManager.instance.score);
    }

    public void OnClickMainMenuButton() =>
        GameManager.LoadGame();
    

    public struct ViewModel
    {
        public int score;
        public int bonus;
        public int level;
    }
}