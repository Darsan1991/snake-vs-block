using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : IList<Vector2>
{
    private readonly List<Vector2> pointList = new List<Vector2>();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    public IEnumerator<Vector2> GetEnumerator() => pointList.GetEnumerator();
    public void Add(Vector2 item) => pointList.Add(item);
    public void Clear() => pointList.Clear();
    public bool Contains(Vector2 item) => pointList.Contains(item);
    public void CopyTo(Vector2[] array, int arrayIndex) => pointList.CopyTo(array, arrayIndex);
    public bool Remove(Vector2 item) => pointList.Remove(item);
    public int Count => pointList.Count;
    public bool IsReadOnly => false;
    public int IndexOf(Vector2 item) => pointList.IndexOf(item);
    public void Insert(int index, Vector2 item) => pointList.Insert(index, item);
    public void RemoveAt(int index) => pointList.RemoveAt(index);

    public Vector2 this[int index]
    {
        get { return pointList[index]; }
        set { pointList[index] = value; }
    }



    public Vector2[] FindSegmentPoints(float segmentLength, int maxCount = -1, float offset = 0f,
        float maxPathLengthOfSegment = Mathf.Infinity)
    {
        if (segmentLength <= 0)
            throw new ArgumentException();

        int baseIndex;
        var startPoint = GetPointForLength(offset, out baseIndex);

        if (pointList.Count == 0 || startPoint == null)
            return null;

        var list = new List<Vector2> {(Vector3) startPoint};
        var lastSelectedPoint = list[list.Count - 1];
        var basePathLength = offset - (lastSelectedPoint - pointList[baseIndex]).magnitude;
        var lastPathLength = offset;
        for (var i = baseIndex + 1; i < pointList.Count; i++)
        {
            var thisPathLength = (pointList[i] - pointList[i - 1]).magnitude;
            var currentInt = 0;
            do
            {
                currentInt++;
                var diffVec = pointList[i] - lastSelectedPoint;

                if (diffVec.sqrMagnitude > segmentLength * segmentLength)
                {
                    var direction = (pointList[i] - pointList[i - 1]).normalized;
                    var relPos = pointList[i - 1] - lastSelectedPoint;

                    #region Equation

                    //                var dis = 0f;
                    //                relPos+direction*dis
                    //
                    //                    (relPos.x + direction.x*dis),(relPos.y + direction.y * dis);
                    //                var eq = relPos.x * relPos.x + 2 * relPos.x * direction.x * dis +
                    //                         relPos.y * relPos.y + 2 * relPos.y * direction.y * dis + 
                    //                         direction.x * direction.x * dis * dis +
                    //                         direction.y * direction.y * dis * dis;
                    //
                    //                (direction.x*direction.x + direction.y*direction.y)*dis*dis 
                    //                    + 2*dis*(relPos.x*direction.x + relPos.y*direction.y)
                    //                    +(relPos.x*relPos.x + relPos.y*relPos.y)

                    #endregion

                    var a = (direction.x * direction.x + direction.y * direction.y);
                    var b = 2 * (relPos.x * direction.x + relPos.y * direction.y);
                    var c = relPos.x * relPos.x + relPos.y * relPos.y - segmentLength * segmentLength;

                    var rootB2Minus4ac = Mathf.Sqrt(b * b - 4 * a * c);
                    //                    Debug.Log("b2:" + b * b + " - 4ac:" + (4 * a * c));
                    var result1 = (-b + rootB2Minus4ac) / (2 * a);
                    var result2 = (-b - rootB2Minus4ac) / (2 * a);

                    var dis = result1 > result2 ? result1 : result2;
                    //                Debug.Log("Distance:"+dis);

                    var targetPoint = relPos + dis * direction + lastSelectedPoint;
                    if ((basePathLength + (targetPoint - pointList[i - 1]).magnitude - lastPathLength) <=
                        maxPathLengthOfSegment)
                    {
                        list.Add(targetPoint);
                        lastSelectedPoint = targetPoint;
                        lastPathLength = basePathLength + (targetPoint - pointList[i - 1]).magnitude;
                        if (list.Count == maxCount)
                        {
                            break;
                        }
                    }
                }

                if ((basePathLength + thisPathLength - lastPathLength) >= maxPathLengthOfSegment)
                {
                    var targetPoint = pointList[i - 1] + (pointList[i] - pointList[i - 1]).normalized *
                                      (maxPathLengthOfSegment - (basePathLength - lastPathLength));
                    list.Add(targetPoint);
                    lastSelectedPoint = targetPoint;
                    lastPathLength += maxPathLengthOfSegment;
                    // basePathLength + (targetPoint - pointList[i - 1]).magnitude;
                    //                                        Debug.Log("Calculated Point:"+targetPoint + " previous:"+pointList[i-1]+" next:"+pointList[i]);
//                    Debug.Log("Base Path Length:" + basePathLength + " this Path Length:" + thisPathLength + " lastPathLength:" + lastPathLength);
                    if (list.Count == maxCount)
                    {
                        break;
                    }
                }
            } while ((pointList[i] - lastSelectedPoint).sqrMagnitude >= segmentLength * segmentLength &&
                     currentInt < 25);
            if (list.Count >= maxCount)
            {
                break;
            }
            basePathLength += thisPathLength;
        }
        if (list.Count == 1)
            list.Clear();
        return list.ToArray();
    }

    Vector2? GetPointForLength(float length, out int baseIndex)
    {
        var currentLength = 0f;

        for (var i = 0; i < pointList.Count - 1; i++)
        {
            var distance = (pointList[i + 1] - pointList[i]).magnitude;
            if (currentLength + distance >= length)
            {
                baseIndex = i;
                return pointList[i] + ((length - currentLength) * (pointList[i + 1] - pointList[i]) / distance);
            }

            currentLength += distance;
        }
        baseIndex = -1;
        return null;
    }
}