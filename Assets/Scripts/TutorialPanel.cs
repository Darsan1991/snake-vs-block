using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPanel : MonoBehaviour,ITutorialController
{

    [SerializeField] private Text tutorialTxt;
    [SerializeField] private float showDeliverPointsDelay;
    public TutorialState tutorialState { get; private set; } = TutorialState.COLLECTING_POINTS;

    private Player player => LevelManager.instance.player;

    private void Start()
    {
        
        SetAndShow("Collect Points to grow snake");
        player.OnGotReward += OnPlayerGotReward;
    }


    private void OnPlayerGotReward(IReward reward)
    {
        player.OnGotReward -= OnPlayerGotReward;
        tutorialState = TutorialState.DELIVERING_POINTS;
        Block.OnTookDamage += OnBlockTookDamage;
        Invoke(nameof(DeliverPointTxt),showDeliverPointsDelay);
    }

    private void DeliverPointTxt()
    {
        Hide(OnFinished:() =>
        {
            SetAndShow("Deliver Points to Get Score");

        });

    }

    private void OnBlockTookDamage(Block block, int damage)
    {
        tutorialState = TutorialState.FINISHED;
        PrefManager.SetKey(PrefManager.SHOWED_TUTORIAL_KEY);
        Hide(OnFinished:()=> StartCoroutine(FinishTutorial()));
        Block.OnTookDamage -= OnBlockTookDamage;
    }

    IEnumerator FinishTutorial()
    {
        SetAndShow("Now You are Ready to Go!");
        yield return new WaitForSeconds(4);
        Hide(OnFinished:()=>{gameObject.SetActive(false);});
    }

    void SetAndShow(string txt, float speed = 1f,Action OnFinished=null)
    {
        var languageWordForWord = ResourceManager.GetLanguageWordForWord(txt);
        tutorialTxt.text = languageWordForWord??txt;
        var color = tutorialTxt.color;
        color.a = 0f;
        tutorialTxt.color = color;
        StartCoroutine(FadeTutorialTxtAlpha(1f, speed,OnFinished));
    }

    void Hide(float speed=1f,Action OnFinished = null)
    {
        StartCoroutine(FadeTutorialTxtAlpha(0, speed, OnFinished));
    }


    private IEnumerator FadeTutorialTxtAlpha(float targetAlpha,float speed=1f,Action OnFinished=null)
    {
        while (Mathf.Abs(targetAlpha - tutorialTxt.color.a) > 0.01f)
        {
            var alpha = Mathf.Lerp(tutorialTxt.color.a,targetAlpha,speed*3f*Time.deltaTime);
            var color = tutorialTxt.color;
            color.a = alpha;
            tutorialTxt.color = color;
            yield return null;
        }
        OnFinished?.Invoke();
    }

}

public interface ITutorialController
{
    TutorialState tutorialState { get; }
}