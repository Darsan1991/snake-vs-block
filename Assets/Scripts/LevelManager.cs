﻿using System;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance { get; private set; }

    public static event Action<int> OnScoreChanged;
    public static event Action<int> OnLevelComplete;
    public static event Action OnGameOver; 
    public static event Action OnGameStart; 

    #region SerizableFields

    [SerializeField] private Player _player;
    [SerializeField] private PointsHandler _pointsHandler;
    [SerializeField] private LevelCreator _levelCreator;
    [SerializeField] private int maxPartCountForLevel = 100;

    #endregion

    #region Properties

    public Player player => _player;
    public PointsHandler pointsHandler => _pointsHandler;
    public LevelCreator levelCreator => _levelCreator;
    public State currentState { get; private set; }
    public int score
    {
        get
        {
            return _score;
        }
        private set
        {
            if (_score!=value)
            {
                _score = value;
                OnScoreChanged?.Invoke(_score);
            }
        }
    }

    public int level => PrefManager.GetCurrentLevel();

    public bool showTutorial => !PrefManager.HasKey(PrefManager.SHOWED_TUTORIAL_KEY);

    public LoadedGameDetails loadedGameDetails => GameManager.loadedGameDetails;

    #endregion

    private int _score;


    void Awake()
    {
        instance = this;
        levelCreator.centerTransform = player.transform;
        levelCreator.StartCreating();
    }

    private void Start()
    {
        if (loadedGameDetails.gameStartState == GameStartState.Restart)
        {
            StartTheGame();
        }
        else if(loadedGameDetails.gameStartState == GameStartState.Continues)
        {
            score = loadedGameDetails.score;
            StartTheGame();
        }
    }

    private void OnEnable()
    {
        pointsHandler.OnReceivePoints += OnReceivePoints;
        player.OnPartCountChanged += OnPlayerPartCountChanged;

    }

    private void OnDisable()
    {
        pointsHandler.OnReceivePoints -= OnReceivePoints;
        player.OnPartCountChanged -= OnPlayerPartCountChanged;
    }

   

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartTheGame();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (currentState == State.PLAYING || currentState == State.LEVEL_COMPLETED || currentState == State.GAME_OVER)
            {
                GameManager.LoadGame();
            }
            else
            {
                var confirmationPanel = UIManager.instance.confirmationPanel;
                confirmationPanel.viewModel = new ConfirmationPanel.ViewModel
                {
                    title = "Exit!",
                    message = "Are you sure want to exit the game?",
                    positiveBtnTitle = "yes",
                    negativeBtnTitle = "no",
                    OnResult = (result) => {
                        if (result)
                        {
                            Application.Quit();
                            Debug.Log("Exiting app");
                        } }
                };
                confirmationPanel.active = true;
            }
        }
    }


    public void StartTheGame()
    {
        if(currentState!=State.WAITING_FOR_PLAY)
            throw new Exception("The current state should be Waiting for play to start the game");
        player.OnPlayerDie += OnPlayerDie;
        player.StartPlay();
        currentState = State.PLAYING;
        OnGameStart?.Invoke();
    }

    private void OnPlayerDie()
    {
        if(currentState!=State.PLAYING)
            throw new Exception("Current State is not playing");
        Invoke(nameof(GameOver),0.1f);
    }

    private void OnPlayerPartCountChanged(int partCount)
    {
        if (partCount >= maxPartCountForLevel && currentState == State.PLAYING)
        {
            LevelComplete();
        }
    }

    void LevelComplete()
    {
        levelCreator.showingLevel = false;
//        player.StopPlay();
        player.OnPlayerDie -= OnPlayerDie;
        levelCreator.EndCreating();
        currentState = State.LEVEL_COMPLETED;
        OnLevelComplete?.Invoke(PrefManager.GetCurrentLevel());
        PrefManager.SetCurrentLevel(PrefManager.GetCurrentLevel()+1);
        
    }

    


    void GameOver()
    {
        player.OnPlayerDie -= OnPlayerDie;
        if(PrefManager.GetBestScore()<score)
            PrefManager.SetBestScore(score);
        currentState = State.GAME_OVER;
        levelCreator.EndCreating();
        OnGameOver?.Invoke();
    }

    private void OnReceivePoints(int points)
    {
        score += points;
    }

    public enum State
    {
        WAITING_FOR_PLAY,PLAYING,LEVEL_COMPLETED,GAME_OVER
    }
}