﻿using UnityEngine;
public class Follower : MonoBehaviour
{
    [SerializeField] protected Transform target;
    [SerializeField] protected Vector3 offset;

    protected virtual void Update()
    {
        transform.position = target.position +  offset;

    }
}