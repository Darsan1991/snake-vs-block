﻿using System;
using UnityEngine;

public class PointsHandler:MonoBehaviour
{
    public Action<int> OnReceivePoints;

    public int currentPoints { get; private set; }

    private void OnEnable()
    {
        Block.OnTookDamage += OnTookDamage;
    }

    private void OnDisable()
    {
        Block.OnTookDamage += OnTookDamage;
    }

    private void OnTookDamage(Block block, int points)
    {
        ReceivePoints(points);
    }

    private void ReceivePoints(int points)
    {
        this.currentPoints += points;
        OnReceivePoints?.Invoke(points);
    }
}