﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTest : MonoBehaviour
{

    [SerializeField] private float speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.Translate(new Vector3(Input.GetAxis("Horizontal")*8*speed * Time.fixedDeltaTime, (1-Input.GetAxis("Vertical")) * speed * Time.fixedDeltaTime, 0));
	}
}