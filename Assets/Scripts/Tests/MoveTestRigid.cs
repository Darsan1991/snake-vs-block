using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class MoveTestRigid : MonoBehaviour
{
    [SerializeField] private Rigidbody2D mRigid;
    [SerializeField] private float speed;

//    private List<Vector3> tempPosList = new List<Vector3>(100000);

    private float xPos;

    // Use this for initialization
    void Start()
    {
        mRigid = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        InputController.OnTouchDrag += OnTouchDrag;
        InputController.OnTouchDown += OnTouchDown;
        InputController.OnTouchUp += OnTouchUp;
    }

    private void OnDisable()
    {
        InputController.OnTouchDrag -= OnTouchDrag;
        InputController.OnTouchDown -= OnTouchDown;
        InputController.OnTouchUp -= OnTouchUp;
    }

    private void OnTouchUp(Vector2 obj)
    {
        xPos = mRigid.position.x;
    }

    private void OnTouchDown(Vector2 obj)
    {
        xPos = mRigid.position.x;
    }


    private void OnTouchDrag(Vector2 obj)
    {
        xPos = xPos + obj.x*5f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        mRigid.velocity = Vector2.zero;
        float lerb = 0.15f;
        var difference = Mathf.Abs(xPos-mRigid.position.x) - 1f;
        if (difference <= 0)
            lerb = 0.15f;
        var x = Mathf.Lerp(mRigid.position.x, xPos, lerb);
        mRigid.MovePosition((new Vector2(x
            , 1 * speed * Time.deltaTime+mRigid.position.y)));
        mRigid.velocity = Vector2.zero;
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        foreach (var contactPoint2D in other.contacts)
        {

            if (Mathf.Abs(Vector2.Dot(contactPoint2D.normal.normalized, Vector2.right)) > 0.5f)
            {
                xPos = mRigid.position.x;
            }
        }
    }
}