﻿using UnityEngine;

public class CollisionTest : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("OnCollisionEnter:" + other.gameObject.name);
    }

    private void OnCollisionExit(Collision other)
    {
        Debug.Log("OnCollisionExit:" + other.gameObject.name);
    }
}