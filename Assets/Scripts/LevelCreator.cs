﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelCreator : MonoBehaviour,ILevelCreator
{

    [SerializeField] private float _maxCoverageDistance;
    [SerializeField] private Transform startPoint;
    [SerializeField] private BlockGroupProvider _blockGroupProvider;

    /// <summary>
    /// Center Transform Of the Environment
    /// </summary>
    public Transform centerTransform { get; set; }
    /// <summary>
    /// Maximum Visible distance from center transform
    /// </summary>
    public float maxCoverageDistance { get { return _maxCoverageDistance; }
        set { _maxCoverageDistance = value; } } 
    /// <summary>
    /// Current State of the LevelCreator
    /// </summary>
    public State currentState { get; private set; }
    public bool creating => currentState == State.CREATING;

    public bool showingLevel
    {
        get { return _showingLevel; }
        set
        {
            if (_showingLevel == value)
                return;
            _showingLevel = value;
            foreach (var cycleTile in tileList)
            {
                ((MonoBehaviour) cycleTile).gameObject.SetActive(value);
            }
        }
    }

    public IBlockGroupProvider blockGroupProvider => _blockGroupProvider;

    private readonly List<ICycleTile> tileList =new List<ICycleTile>();
    private bool _showingLevel;

    private void Awake()
    {
        showingLevel = true;
    }

    private void Update()
    {
        if (currentState == State.CREATING)
        {
            if (tileList.Count == 0 ||
                (tileList[tileList.Count - 1].position.y + tileList[tileList.Count - 1].height - centerTransform.position.y) <
                maxCoverageDistance)
            {
                Vector3 targetPos = tileList.Count > 0 ? tileList[tileList.Count - 1].position + Vector2.up * tileList[tileList.Count - 1].height : (Vector2)startPoint.position;
                targetPos.z = transform.position.z;
                var selectedPrefab = blockGroupProvider.GetBlockGroup();
                Debug.Log("Selected Prefab:"+selectedPrefab);
                var cycleTile = Instantiate(selectedPrefab).GetComponent<ICycleTile>();
                cycleTile.SetPosition(targetPos);
                ((MonoBehaviour)cycleTile).gameObject.SetActive(showingLevel);
                tileList.Add(cycleTile);
            }

            if (tileList.Count > 0 &&
                (centerTransform.position.y - (tileList[0].position.y +tileList[0].height)) > maxCoverageDistance)
            {
                var cycleTile = tileList[0];
                tileList.RemoveAt(0);
                var monoBehavior = cycleTile as MonoBehaviour;
                if (monoBehavior != null) Destroy(monoBehavior.gameObject);
            }
        }
    }

    #region ILevelCreator

    public void StartCreating()
    {
        if(!centerTransform)
            throw new Exception("Set the Center Transform before Start Creating");
        if (currentState==State.CREATING)
        {
            throw new Exception("Already in Creating state");
        }

        currentState = State.CREATING;
    }



    public void EndCreating()
    {
        currentState = State.NONE;
    }

    #endregion

    #region Inner Class,Enums,Structs

    public enum State
    {
        NONE,CREATING
    }

    #endregion
}


public interface IBlockGroupProvider
{
    GameObject GetBlockGroup();
}

public interface ILevelCreator
{
    bool showingLevel { get; set; }
    bool creating { get; }
    /// <summary>
    /// Call to Start Crete The level
    /// </summary>
    void StartCreating();
    /// <summary>
    /// Call to Stop Create the level
    /// </summary>
    void EndCreating();
}