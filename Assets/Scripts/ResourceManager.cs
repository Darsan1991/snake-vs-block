﻿using System;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager instance { get; private set; }

    [SerializeField] private Color[] _colors;
    [SerializeField] private VecInt _minAndBlockValue;
    [SerializeField] private TextScriptable textScriptable;
    [SerializeField] private LanguageScriptable _languageScriptable;

    public static Color[] blockColors => instance._colors;
    public static VecInt minAndBlockValue => instance._minAndBlockValue;
    public bool inilized { get; private set; }
    public static LanguageScriptable languageScriptable => instance._languageScriptable;
    public static Language currentLanguage => PrefManager.GetCurrentLanguage();
    public static bool Inilized => instance!=null && instance.inilized;
    public static IReadOnlyCollection<LanguageAndName> languagesAndNames => instance._languageScriptable
        .languagesAndNames;
    private readonly Dictionary<string,WordLanguageGroup> wordVsLanguageGroupDict = new Dictionary<string, WordLanguageGroup>();
    
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        Init();
    }

    public void Init()
    {
        if (inilized)
        {
            return;
        }
        var languageGroups = textScriptable.wordLanguageGroups;
        foreach (var wordLanguageGroup in languageGroups)
        {
            var lower = wordLanguageGroup.word.ToLower();
            if(wordVsLanguageGroupDict.ContainsKey(lower))
                throw new Exception("The Word is already there:"+wordLanguageGroup.word);
           wordVsLanguageGroupDict.Add(lower, wordLanguageGroup);
        }
        inilized = true;
        Debug.Log("Inilized");
    }

    public static Color GetColorForValue(int val) => Utils.ValueToColor(val, minAndBlockValue, blockColors);

    public static string GetLanguageWordForWord(string word)
    {
        
        var dict = instance.wordVsLanguageGroupDict;
        var lower = word.ToLower();
//        Debug.Log(nameof(GetLanguageWordForWord) + ":"+lower+" contains:"+dict.ContainsKey(lower)+" dict count:"+dict.Count);

        if (dict.ContainsKey(lower))
        {
            return dict[lower].GetWordForLanguage(currentLanguage);
        }

        return null;
    }
   
}