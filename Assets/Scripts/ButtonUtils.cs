using UnityEngine;
using UnityEngine.SceneManagement;

public static class GeneralButtons
{
    public static void RatingButton()
    {
        Application.OpenURL("market://details?id="+Application.identifier);

    }

    public static void TopScoreButton()
    {
//        GooglePlayScript.instance.ShowLeadersboard();
    }

    public static void ShareButton()
    {
        string screenShotPath = Application.persistentDataPath + "/" + "screenshot.png";
        ScreenCapture.CaptureScreenshot("screenshot.png");

        Share("Hey Check this awasome game\n" +
              "https://play.google.com/store/apps/details?id="+Application.identifier, screenShotPath, "");
    }

    static void Share(string shareText, string imagePath, string url, string subject = "")
    {
#if UNITY_ANDROID
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
        AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
        intentObject.Call<AndroidJavaObject>("setType", "image/png");

        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

        AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
        currentActivity.Call("startActivity", jChooser);
#endif
    }

    public static void SoundButton()
    {
        PrefManager.SetSound(!PrefManager.IsSound());
    }
}