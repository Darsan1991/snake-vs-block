﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class AutoCameraSizer : MonoBehaviour
{
    [SerializeField]private Mode mode;
    [SerializeField]private float size;
    private Camera mCamera;


    private void Awake()
    {
        mCamera = GetComponent<Camera>();
        if (mode == Mode.VERTICAL_FIT)
        {
            mCamera.orthographicSize = size / 2;
        }
        else
        {
            var aspectRatio = (Screen.height+0f)/Screen.width;
            mCamera.orthographicSize = aspectRatio * size * 0.5f;
        }
    }

    public enum Mode
    {
        VERTICAL_FIT,HORIZONTAL_FIT
    }
}
