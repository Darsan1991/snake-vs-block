﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour {
    public static ObjectPoolManager instance;

    [SerializeField] private List<PoolObjSet> poolObjSets;

    private readonly Dictionary<PoolObjType,PoolObjGroup> typeVsPoolObjDict = new Dictionary<PoolObjType, PoolObjGroup>();
    private bool initialized;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if(instance!=this)
        {
            Destroy(gameObject);
        }
        Inilize();
    }

    void Inilize()
    {
        if(initialized)
            return;

        foreach (var poolObjSet in poolObjSets)
        {
           

            var objWithId = poolObjSet.poolObjPrefab as PoolObjWithId;
            if (objWithId != null)
            {
                if (!typeVsPoolObjDict.ContainsKey(poolObjSet.poolObjPrefab.GetPoolObjType()))
                {
                    typeVsPoolObjDict.Add(poolObjSet.poolObjPrefab.GetPoolObjType(),
                        new GroupPoolGroup(poolObjSet.poolObjPrefab.GetPoolObjType()));
                }
                var groupPoolGroup = (GroupPoolGroup) typeVsPoolObjDict[poolObjSet.poolObjPrefab.GetPoolObjType()];
                if (!groupPoolGroup.idVsPoolObjDict.ContainsKey(objWithId.id))
                {
                    groupPoolGroup.idVsPoolObjDict.Add(objWithId.id, new Queue<PoolObj>());
                }
            }
            else
            {
                if (!typeVsPoolObjDict.ContainsKey(poolObjSet.poolObjPrefab.GetPoolObjType()))
                {
                    typeVsPoolObjDict.Add(poolObjSet.poolObjPrefab.GetPoolObjType(),
                        new SinglePoolGroup(poolObjSet.poolObjPrefab.GetPoolObjType()));
                }
            }
        }

        initialized = true;

        CreateObjectAtStart();
    }

    public int[] GetAllIdsOfThePoolType(PoolObjType poolObjType)
    {
        if (typeVsPoolObjDict.ContainsKey(poolObjType))
        {
            if (typeVsPoolObjDict[poolObjType].GetPoolObjType()==PoolObjType.BLOCK_GROUP)
            {
                return ((GroupPoolGroup) typeVsPoolObjDict[poolObjType]).idVsPoolObjDict.Keys.ToArray();
            }
        }

        return null;
    }
    

    void CreateObjectAtStart()
    {
        foreach (var poolObjSet in poolObjSets)
        {
            CreatePoolObject(poolObjSet.poolObjPrefab,poolObjSet.startCount);
        }
    }


    public static void DestroyPoolObj(PoolObj poolObj)
    {
        var poolObjType = poolObj.GetPoolObjType();
        var poolObjDict = instance.typeVsPoolObjDict;
        if (poolObjDict.ContainsKey(poolObjType))
        {
            poolObj.OnPoolObjDestroy();
            poolObj.gameObject.SetActive(false);
            poolObj.transform.parent = instance.transform;
            poolObjDict[poolObjType].AddPoolObj(poolObj);
        }
    }

    public static void DestroyPoolObj(GameObject gameObject)
    {
        DestroyPoolObj(gameObject.GetComponent<PoolObj>());
    }


    public static PoolObj InstantiatePoolObj(PoolObjType type, Vector3 position, Quaternion rotation,int id=-1)
    {
        var poolObjDict = instance.typeVsPoolObjDict;
        if(!poolObjDict.ContainsKey(type))
            throw new Exception("PoolType not Found:"+type);
        PoolObj poolObj;
        if (poolObjDict[type].GetGroupType()==PoolObjGroup.GroupType.WITH_SUB_GROUP)
        {
            var groupPoolGroup = (GroupPoolGroup)poolObjDict[type];

            int poolObjKey;

            if (id == -1)
            {
                var keys = new List<int>(groupPoolGroup.idVsPoolObjDict.Keys);
                poolObjKey = keys[UnityEngine.Random.Range(0, keys.Count)];
            }
            else
            {
                poolObjKey = id;
            }


            var poolObjs = groupPoolGroup.idVsPoolObjDict[poolObjKey];

           
            if(poolObjs.Count == 0)
            instance.CreatePoolObject(instance.GetPoolPrefab(type,poolObjKey));

            poolObj = poolObjs.Dequeue();
        }
        else
        {
            var singlePoolGroup = (SinglePoolGroup)poolObjDict[type];
            if (singlePoolGroup.poolObjList.Count==0)
            {
                instance.CreatePoolObject(instance.GetPoolPrefab(type));
            }

           poolObj = singlePoolGroup.poolObjList.Dequeue();
        }

        instance.MakeItReadyAsInstantiate(poolObj,position,rotation);

        return poolObj;

    }

    void MakeItReadyAsInstantiate(PoolObj poolObj,Vector3 position,Quaternion rotation)
    {
        poolObj.transform.parent = null;
        poolObj.transform.position = position;
        poolObj.transform.rotation = rotation;
        poolObj.OnPoolObjInstantiate();
        poolObj.gameObject.SetActive(true);
    }

    PoolObj GetPoolPrefab(PoolObjType type, int id = -1)
    {
        PoolObj poolPrefab;
        if (id == -1)
        {
            var i = poolObjSets.FindIndex(obj=>obj.poolObjPrefab.GetPoolObjType() == type);
            poolPrefab = poolObjSets[i].poolObjPrefab;
        }
        else
        {
            var poolGroup = poolObjSets.FindAll(obj => obj.poolObjPrefab.GetPoolObjType() == type);

            var i = poolGroup.FindIndex(obj =>
            {
                var objWithId = obj.poolObjPrefab as PoolObjWithId;

                if (objWithId != null)
                    return objWithId.id == id;

                return false;
            });

            poolPrefab = poolObjSets[i].poolObjPrefab;
        }

        return poolPrefab;

    }

    void CreatePoolObject(PoolObj poolObj,int count=1)
    {
        if(!initialized)
            throw new Exception("Object Pool Initialized before create Pool Object");

        for (int i = 0; i < count; i++)
        {
            var newPoolObj = Instantiate(poolObj);
            newPoolObj.transform.parent = transform;
            newPoolObj.Inilize();
            newPoolObj.gameObject.SetActive(false);

            if (!typeVsPoolObjDict.ContainsKey(poolObj.GetPoolObjType()))
            {
                throw new Exception("This Type not Initialized");
            }

            typeVsPoolObjDict[poolObj.GetPoolObjType()].AddPoolObj(newPoolObj);

        }

    }


    [Serializable]
    public struct PoolObjSet
    {
        public PoolObj poolObjPrefab;
        public int startCount;
        public int maxCount;
    }


    public class SinglePoolGroup:PoolObjGroup
    {
       
        public readonly Queue<PoolObj> poolObjList = new Queue<PoolObj>();
        public override GroupType GetGroupType()
        {
            return GroupType.SINGLE;
        }

       public SinglePoolGroup(PoolObjType poolObjType):base(poolObjType)
       {
          
       }

        public override void AddPoolObj(PoolObj poolObj)
        {
            poolObjList.Enqueue(poolObj);
        }
    }

    public class GroupPoolGroup:PoolObjGroup
    {
        public readonly Dictionary<int,Queue<PoolObj>> idVsPoolObjDict = new Dictionary<int, Queue<PoolObj>>();

        public GroupPoolGroup(PoolObjType poolObjType) : base(poolObjType)
        {
        }

        public override GroupType GetGroupType()
        {
            return GroupType.WITH_SUB_GROUP;
        }

        public override void AddPoolObj(PoolObj poolObj)
        {
            var poolObjWithId = poolObj as PoolObjWithId;
            if(!poolObjWithId)
                throw new Exception("Only ObjectTypeWith Id can Add to GroupPoolGroup");
            
            if (!idVsPoolObjDict.ContainsKey(poolObjWithId.id))
            {
                idVsPoolObjDict.Add(poolObjWithId.id,new Queue<PoolObj>());
            }
            idVsPoolObjDict[poolObjWithId.id].Enqueue(poolObj);
        }
    }

    public abstract class PoolObjGroup
    {
        private readonly PoolObjType poolObjType;
        public abstract GroupType GetGroupType();

        public enum GroupType
        {
            SINGLE,WITH_SUB_GROUP
        }

        protected PoolObjGroup(PoolObjType poolObjType)
        {
            this.poolObjType = poolObjType;
        }

        public PoolObjType GetPoolObjType()
        {
            return poolObjType;
        }

        public abstract void AddPoolObj(PoolObj poolObj);

    }

}

public interface IInitializable
{
    bool inilized { get; }
    void Init();
}

public interface IInitializable<T>
{
    bool inilized { get; }

    void Init(T t);
}

