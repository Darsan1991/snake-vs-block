﻿using System;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public const float MAX_DISTANCE_CAN_MOVE_AT_CLICK = 1f;
    public const float MAX_TIME_THAT_CAN_DETECT_AS_CLICK = 0.4f;
    public const float MAX_TIME_BETWEEN_CLICK = 0.4f;

    public static event Action<Vector2> OnTouchDown;
    public static event Action<Vector2> OnTouchDrag;
    public static event Action<Vector2> OnTouchUp;
    public static event Action OnDoubleTouch;

    private bool isDragging;
    private Vector2 lastPostion;
    private Vector2 clickedPos;
    private int clickCount;
    private float clickedUpTime;
    private float clickDownTime;
    private bool isDoubleClicked;


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isDragging = true;
            var worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            lastPostion = (Vector2)worldPoint;
            clickedPos = worldPoint;

            if (clickCount > 0)
            {
                if (Time.time - clickedUpTime > MAX_TIME_BETWEEN_CLICK)
                {
                    clickCount = 0;
                }
            }

            clickDownTime = Time.time;
            OnTouchDown?.Invoke(worldPoint);

        }

        if (isDragging)
        {
            var currentPos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);

            OnTouchDrag?.Invoke(currentPos - lastPostion);

            lastPostion = currentPos;
        }

        if (Input.GetMouseButtonUp(0))
        {
            clickedUpTime = Time.time;

           
            isDragging = false;


            var worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            OnTouchUp?.Invoke(worldPoint);
            if (
                (clickedPos - (Vector2) worldPoint).sqrMagnitude <
                MAX_DISTANCE_CAN_MOVE_AT_CLICK * MAX_DISTANCE_CAN_MOVE_AT_CLICK
                && (Time.time - clickDownTime) < MAX_TIME_THAT_CAN_DETECT_AS_CLICK
            )
            {
                clickCount++;
                if (clickCount == 2)
                {
                    clickCount = 0;

                    OnDoubleTouch?.Invoke();
                }
            }
        }
    }
}