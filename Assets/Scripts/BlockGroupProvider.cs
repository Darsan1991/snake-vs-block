using UnityEngine;

public class BlockGroupProvider : MonoBehaviour,IBlockGroupProvider
{
    [SerializeField] private MonoBehaviour[] collectingBlockGroupPrefabs;
    [SerializeField] private MonoBehaviour[] deliveringBlockGroupPrefabs;
    [SerializeField] private MonoBehaviour[] normalBlockGroupPrefabs;
    [SerializeField] private TutorialPanel tutorialPanel;

    public ITutorialController tutorialController => tutorialPanel;

    private LevelManager levelManager;



    public GameObject GetBlockGroup()
    {
//        Debug.Log(nameof(GetBlockGroup));

        levelManager = LevelManager.instance;
        if (levelManager.showTutorial)
        {
            switch (tutorialController.tutorialState)
            {
                case TutorialState.COLLECTING_POINTS:
                    return collectingBlockGroupPrefabs[Random.Range(0, collectingBlockGroupPrefabs.Length)].gameObject;

                    case  TutorialState.DELIVERING_POINTS:
                        return deliveringBlockGroupPrefabs[Random.Range(0, deliveringBlockGroupPrefabs.Length)].gameObject;

            }
        }
        return normalBlockGroupPrefabs[Random.Range(0, normalBlockGroupPrefabs.Length)].gameObject;
    }
}