﻿using System.Collections.Generic;
using UnityEngine;

public abstract class PoolObj:MonoBehaviour,IPoolObj
{
    protected List<IPoolObjEventReciever> poolObjEventRecievers = new List<IPoolObjEventReciever>();
    public virtual void Inilize()
    {
        poolObjEventRecievers = new List<IPoolObjEventReciever>(GetComponentsInChildren<IPoolObjEventReciever>());
    }

    public virtual void OnPoolObjInstantiate()
    {
        for (var i = 0; i < poolObjEventRecievers.Count; i++)
        {
            poolObjEventRecievers[i].OnPoolObjInstantiate();
        }
    }

    public virtual void OnPoolObjDestroy()
    {
        for (var i = 0; i < poolObjEventRecievers.Count; i++)
        {
            poolObjEventRecievers[i].OnPoolObjDestroy();
        }
    }

    public abstract PoolObjType GetPoolObjType();
}

public interface IPoolObj:IUnityObj
{
    void Inilize();
    void OnPoolObjInstantiate();
    void OnPoolObjDestroy();
    PoolObjType GetPoolObjType();
}

public interface IUnityObj
{
    Transform transform { get; }
    GameObject gameObject { get; }
}