﻿using UnityEngine;

public class ScorePlayerFollower : Follower
{
    [SerializeField] private Player player;
    [SerializeField] private TextMesh textMesh;

    private int partCount
    {
        get
        {
            return _partCount;
        }
        set
        {
            _partCount = value;
            textMesh.text = partCount.ToString();
        }
    }

    private int _partCount;

    private void OnEnable()
    {
        player.OnPartCountChanged += OnPlayerPartCountChanged;
        player.OnPlayerDie += OnPlayerDie;
        partCount = player.partCount;
        
    }

    private void OnPlayerDie()
    {
        textMesh.gameObject.SetActive(false);
    }

    private void OnPlayerPartCountChanged(int count) => partCount = count;
    

    private void OnDisable()
    {
        if (player!=null)
        {
            player.OnPartCountChanged -= OnPlayerPartCountChanged;
            player.OnPlayerDie -= OnPlayerDie;
        }
    }
}