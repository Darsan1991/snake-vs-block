﻿using System;

[Serializable]
public struct VecInt
{
    public int x, y;
}