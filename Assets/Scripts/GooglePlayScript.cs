﻿using GooglePlayGames;
using UnityEngine;

public class GooglePlayScript : MonoBehaviour
{
    public static GooglePlayScript instance;
    public bool isInit;
    public bool isLogIn;

    [SerializeField] private string leadersboardId;
    // Use this for initialization

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        Init();
        DontDestroyOnLoad(gameObject);
    }

    private void OnEnable()
    {
        PrefManager.OnBestScoreChanged += OnHighScoreChanged;
    }

    private void OnDisable()
    {
        PrefManager.OnBestScoreChanged -= OnHighScoreChanged;
    }

    private void OnHighScoreChanged(int score)
    {
        SubmitTopScoreOnLeadersBoard(score);
    }

    void Start()
    {
        SignIn();
    }

    void Init()
    {
        if (isInit)
            return;
        PlayGamesPlatform.Activate();
        isInit = true;
    }

    public void SignIn()
    {
        if (!isInit)
        {
            Init();
            return;
        }
        Social.localUser.Authenticate((bool success) =>
        {
            // handle success or failure
            if (success)
            {
                isLogIn = true;
            }

        });
    }

    public void ShowLeadersboard()
    {
        if (!isLogIn)
        {
            SignIn();
            return;
        }

        Social.ShowLeaderboardUI();
    }

    public void SubmitTopScoreOnLeadersBoard(int score)
    {
        if (!isLogIn)
        {
            return;
        }
        Social.ReportScore(score,leadersboardId, success =>
          {
              // handle success or failure
          });

    }

}
