﻿using GoogleMobileAds.Api;
using UnityEngine;
using System;

public class AdmobController : MonoBehaviour
{
    public static AdmobController instance;

    [SerializeField] private float minTimeBetweenAds;
    [SerializeField] private int minGameOverBetweenAds;

    InterstitialAd interstitial;
    bool isInit;

    private int nextInterstitialGameOver;
    private float nexInterstitialTime;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }


    // Use this for initialization
    void Start()
    {
        Inilize();
    }

    private void OnEnable()
    {

            LevelManager.OnGameOver += OnGameOver;

    }


    private void OnDisable()
    {

      LevelManager.OnGameOver -= OnGameOver;

    }

    private void OnGameOver()
    {
        nextInterstitialGameOver--;
        if(nextInterstitialGameOver<=0&&Time.time>=nexInterstitialTime)
            ShowInterstitial();
    }

    void Inilize()
    {

        if (isInit)
            return;
        nexInterstitialTime = Time.time+minGameOverBetweenAds;
        nextInterstitialGameOver = minGameOverBetweenAds;
        RequestInterstitial();
        isInit = true;
    }



 

    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-9090942574524127/6688407495";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-9090942574524127/2118607092";
#else
        string adUnitId = "unexpected_platform";
#endif


        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        interstitial.OnAdClosed += OnAdsClosed;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }

    private void OnAdsClosed(object sender, EventArgs e)
    {
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
        nexInterstitialTime = Time.time + minTimeBetweenAds;
        nextInterstitialGameOver = minGameOverBetweenAds;
    }


    public void ShowInterstitial()
    {
        if (interstitial.IsLoaded())
            interstitial.Show();

        Debug.Log("Show Interstitial");
    }

}