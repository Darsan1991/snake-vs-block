﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Collider2D))]
public class Block : MonoBehaviour
{
    public static event Action<Block,int> OnTookDamage;

    private static IDamageable currentDamageable;


    #region SerizableField

    [SerializeField] private VecInt minAndMaxValue;
    [SerializeField] private TextMesh textMesh;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private float timeBetweenDamage;
    [SerializeField] private Collider2D _mCollider;
    [SerializeField] private GameObject dieParticleEffectPrefab;

    #endregion

    #region Properties

    /// <summary>
    /// Alpha of the block this will apply to all block members
    /// </summary>
    public float alpha
    {
        get { return spriteRenderer.color.a; }
        set
        {
            var textMeshColor = textMesh.color;
            textMeshColor.a = value;
            textMesh.color = textMeshColor;

            var spriteRendererColor = spriteRenderer.color;
            spriteRendererColor.a = value;
            spriteRenderer.color = spriteRendererColor;
        }
    }

    /// <summary>
    /// Points value
    /// </summary>
    public int value
    {
        get { return _value; }
        set
        {
            textMesh.text = value.ToString();
            _value = value;
            color = ResourceManager.GetColorForValue(value);
        }
    }

    #region Private Properties

    /// <summary>
    /// Color of the block
    /// </summary>
    private Color color { get { return spriteRenderer.color; } set { spriteRenderer.color = value; } }

    /// <summary>
    /// This Collider
    /// </summary>
    private Collider2D mCollider
    {
        get
        {
            if (!_mCollider)
            {
                _mCollider = GetComponent<BoxCollider2D>();
            }

            return _mCollider;
        }
    }

    /// <summary>
    /// Currently Damaging damageable object
    /// </summary>
    private IDamageable damageable
    {
        get
        {
            return _damageable;
        }
        set
        {

            StopAllCoroutines();
            _damageable = value;
            if (_damageable != null)
                StartCoroutine(DamageCor(() =>
                {
                    _damageable = null;
                    if (this.value <= 0)
                    {
                        Die();
                    }

                }));
        }
    }


    private IDamageable _damageable
    {
        get
        {
            return __damageable;
        }
        set
        {
            __damageable = value;
            currentDamageable = value;
        }
    }

    #endregion

    #endregion

    private float lastDamageTime;
    private int _value;
    /// <summary>
    /// For Make Only OneBlock can damage
    /// </summary>
    private IDamageable __damageable;



    #region Unity Callbacks

    private void Start()
    {
        value = Random.Range(minAndMaxValue.x, minAndMaxValue.y);
    }

    #region Collision Callback

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(currentDamageable!=null)
            return;
        if (other.gameObject.layer == 8)
        {
            damageable = other.gameObject.GetComponent<IDamageable>();
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (currentDamageable != null)
            return;
        if (other.gameObject.layer == 8)
        {
            damageable = other.gameObject.GetComponent<IDamageable>();
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.layer == 8)
        {
            if (damageable != null && other.collider.gameObject == ((MonoBehaviour)damageable).gameObject)
            {
                damageable = null;
            }
        }
    }

    #endregion
    

    #endregion


    private void Die()
    {
        mCollider.enabled = false;
        alpha = 0f;
        var go = Instantiate(dieParticleEffectPrefab,transform.position,Quaternion.identity);
        Destroy(go,4);
        Destroy(gameObject);
    }


    private IEnumerator DamageCor(Action OnFinished=null)
    {
        var seedTime = Time.time - (lastDamageTime + timeBetweenDamage);
        if (seedTime<0)
            yield return new WaitForSeconds(-seedTime);

        while (true)
        {
            if (damageable.value > 0 && value > 0)
            {
                TookDamage();
                lastDamageTime = Time.time;
                if (damageable.value > 0 && value > 0)
                    yield return new WaitForSeconds(timeBetweenDamage);
            }
            else
            {
                break;
            }
        }
        OnFinished?.Invoke();

    }

    private void TookDamage()
    {
        value--;
        damageable.Damage();
//        Debug.Log("balance :"+damageable.value);
        OnTookDamage?.Invoke(this,1);
    }

  
}