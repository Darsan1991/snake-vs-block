﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private const int MIN_COUNT_FOR_LEVEL = 30;
    private const int MIN_COUNT_AFTER_CURRENT_LEVEL = 20;
    public static GameManager instance { get; private set; }

    public static LoadedGameDetails loadedGameDetails { get; private set; } = new LoadedGameDetails{gameStartState = GameStartState.New};

    public static Level[] GetAllLevels()
    {
        var currentLevel = PrefManager.GetCurrentLevel();
        var totalLevelCount = Mathf.Max(currentLevel + MIN_COUNT_AFTER_CURRENT_LEVEL, MIN_COUNT_FOR_LEVEL);
        var levelList = new List<Level>();
        for (int i = 1; i < totalLevelCount+1; i++)
        {
            var level = new Level(i,i>currentLevel);
            levelList.Add(level);
        }
        return levelList.ToArray();
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public static void LoadGame(GameStartState gameStartState = GameStartState.New,int lastScore=0)
    {
        loadedGameDetails = new LoadedGameDetails { gameStartState = gameStartState,score = lastScore};
        SharedUIManager.ShowLoadingScreen();
        SceneManager.LoadScene("Main");
    }
}

public struct LoadedGameDetails
{
    public GameStartState gameStartState;
    public int score;
}