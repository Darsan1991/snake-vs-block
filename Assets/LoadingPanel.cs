using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPanel : MonoBehaviour
{
    public event Action OnHide;
    public event Action OnShow;
    [SerializeField] private Image img;

    public bool showing { get { return gameObject.activeSelf; } private set{gameObject.SetActive(value);}}

    private float alpha { get { return img.color.a; }  set
    {
        var color = img.color;
        color.a = value;
        img.color = color;
    } }


    public void Show()
    {
        showing = true;
        StartCoroutine(Fade(1, 0,Hide));
        OnShow?.Invoke();
    }

    IEnumerator Fade(float startA,float endA,Action OnFinished=null)
    {
        alpha = startA;
        while (Mathf.Abs(endA-alpha)>0.001f)
        {
            alpha= Mathf.MoveTowards(alpha, endA, 1 * Time.deltaTime);
            yield return null;
        }
        OnFinished?.Invoke();
    }

    public void Hide()
    {
        showing = false;
        OnHide?.Invoke();
    }
}