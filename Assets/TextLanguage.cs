using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextLanguage : MonoBehaviour
{
    [SerializeField] private string word;
    [SerializeField]private WordType wordType = WordType.NONE;
    private Text text;

    private void Awake()
    {
        if (!text)
        {
            text = GetComponent<Text>();
        }
        SetText();
    }

    private void OnEnable()
    {
        PrefManager.OnLanguageChanged += OnLanguageChanged;
    }

    private void OnDisable()
    {
        PrefManager.OnLanguageChanged -= OnLanguageChanged;

    }

    private void OnLanguageChanged(Language language)
    {
        SetText();
    }

    private void SetText()
    {
        if (ResourceManager.Inilized)
        {
            var languageWordForWord = ResourceManager.GetLanguageWordForWord(word);
            Debug.Log(nameof(SetText)+languageWordForWord);
            if (languageWordForWord!=null)
            {
                switch (wordType)
                {
                        
                    case WordType.FIRST_UPPER:
                        languageWordForWord = FirstLetterUppercase(languageWordForWord);
                        break;

                        case WordType.LOWER:
                            languageWordForWord = languageWordForWord.ToLower();
                        break;

                        case WordType.UPPER:
                            languageWordForWord = languageWordForWord.ToUpper();
                        break;
                }
            }
            text.text = languageWordForWord ?? text.text;
        }
        else
        {
            LateCall.Create().Call(()=>ResourceManager.Inilized,SetText);
        }
    }

    public static string FirstLetterUppercase(string str)
    {
        if (string.IsNullOrEmpty(str))
        {
            return null;
        }

        return char.ToUpper(str[0]) + str.Substring(1);

    }

    public enum WordType
    {
        UPPER,LOWER,FIRST_UPPER,NONE
    }
}