using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Language Scriptable",menuName = "Scriptable/Language Scriptable")]
public class LanguageScriptable : ScriptableObject
{
    [SerializeField] private List<LanguageAndName> _languageAndNames;
    public IReadOnlyCollection<LanguageAndName> languagesAndNames => _languageAndNames.AsReadOnly();
}