using UnityEngine;
using UnityEngine.EventSystems;

public class PlaySoundAtClick : MonoBehaviour,IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        AudioManager.Play(AudioManager.ON_CLICK_CLIP);
    }
}