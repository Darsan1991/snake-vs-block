﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager instance { get; private set; }

    #region SerizableFields

    [SerializeField] private List<MonoBehaviour> initializables = new List<MonoBehaviour>();
    [SerializeField] private GameOverPanel _gameOverPanel;
    [SerializeField] private GamePlayPanel _gamePlayPanel;
    [SerializeField] private MenuPanel _MenuPanel;
    [SerializeField] private LanguagePanel _languagePanel;
    [SerializeField] private TutorialPanel _tutorialPanel;
    [SerializeField] private LevelCompletePanel _levelCompletePanel;
    [SerializeField] private LevelPanel _levelPanel;
    [SerializeField] private ConfirmationPanel _confirmationPanel;

    #endregion

    #region Properties

    public ConfirmationPanel confirmationPanel => _confirmationPanel;
    public GameOverPanel gameOverPanel => _gameOverPanel;
    public GamePlayPanel gamePlayPanel => _gamePlayPanel;
    public MenuPanel menuPanel => _MenuPanel;
    public LanguagePanel languagePanel => _languagePanel;
    public TutorialPanel tutorialPanel => _tutorialPanel;
    public LevelCompletePanel levelCompletePanel => _levelCompletePanel;
    public LevelPanel levelPanel => _levelPanel;
    #endregion

    private void Awake()
    {
        instance = this;
        if (!PrefManager.HasKey(PrefManager.SETTLED_LANGUAGE_KEY))
        {
            languagePanel.gameObject.SetActive(true);
        }

        LateCall.Create().Call(()=>LevelManager.instance, () =>
        {
            SetupForStart(LevelManager.instance.loadedGameDetails.gameStartState);
        });

        foreach (var monoBehaviour in initializables)
        {
            var initializable = (IInitializable)monoBehaviour;
            initializable.Init();
        }
    }

    private void SetupForStart(GameStartState gameStartState)
    {
        if (gameStartState == GameStartState.New)
        {
            menuPanel.Show(false);

        }
        else
        {
         menuPanel.Hide(false);

        }
    }

    private void OnEnable()
    {
        LevelManager.OnGameOver += OnGameOver;
        LevelManager.OnGameStart += OnGameStart;
        LevelManager.OnLevelComplete += OnLevelComplete;
    }

    

    public void OnDisable()
    {
        LevelManager.OnGameOver -= OnGameOver;
        LevelManager.OnGameStart -= OnGameStart;
        LevelManager.OnLevelComplete -= OnLevelComplete;
    }

    private void OnLevelComplete(int level)
    {
        levelCompletePanel.viewModel = new LevelCompletePanel.ViewModel
        {
            score = LevelManager.instance.score,
            bonus = LevelManager.instance.player.partCount+1,
            level = level
        };
        gamePlayPanel.gameObject.SetActive(false);
        levelCompletePanel.Show();
        Invoke(nameof(HideLevelCompletePanelAndLoadNextLevel),4f);
    }

    void HideLevelCompletePanelAndLoadNextLevel()
    {
        levelCompletePanel.Hide(OnFinished: () =>
        {
            GameManager.LoadGame(GameStartState.Continues, levelCompletePanel.viewModel.score+levelCompletePanel.viewModel.bonus);

        });

    }

    void LoadNextLevel()
    {
    }

    private void OnGameStart()
    {
        if(LevelManager.instance.showTutorial)
            tutorialPanel.gameObject.SetActive(true);
        if (menuPanel.showing)
        {
            menuPanel.Hide(LevelManager.instance.loadedGameDetails.gameStartState==GameStartState.New);
        }
        gamePlayPanel.gameObject.SetActive(true);
    }

    

    private void OnGameOver()
    {
        gamePlayPanel.gameObject.SetActive(false);
        gameOverPanel.Init(new GameOverPanel.InitParams
        {
            score = LevelManager.instance.score,
            bestScore = PrefManager.GetBestScore() 
        });
        gameOverPanel.Activate();
    }

    private void OnValidate()
    {
        for (var i = 0; i < initializables.Count; i++)
        {
            if (!(initializables[i] is IInitializable))
            {
                var initializable = initializables[i].gameObject.GetComponent<IInitializable>();

                initializables.RemoveAt(i);

                if (initializable != null)
                {
                    initializables.Insert(i,(MonoBehaviour)initializable);
                }
                else
                {
                    i--;
                }

                   
            }
        }
    }
}