using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SharedUIManager : MonoBehaviour
{

    public static SharedUIManager instance
    {
        get
        {
            if (_instance == null)
            {
                lock (lockObj)
                {
                    if (_instance == null)
                    {
                        
                        _instance = Instantiate(Resources.Load<GameObject>("SharedUIManager")).GetComponent<SharedUIManager>();
                        DontDestroyOnLoad(_instance);
                    }
                }
            }

            return _instance;
        }

        set { _instance = value; }
    }

    private static SharedUIManager _instance;

    [SerializeField] private LoadingPanel _loadingPanel;

 
    public LoadingPanel loadingPanel => _loadingPanel;



    private static object lockObj = new object();


    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(_instance);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    public static void ShowLoadingScreen(Action OnFinished = null)
    {
        Action act = null;
        act = () =>
        {
            instance.loadingPanel.OnHide -= act;
//            Debug.Log("Act:"+act);
            OnFinished?.Invoke();
        };
        instance.loadingPanel.OnHide += act;
//        loadingFinishedCallback = OnFinished;
        instance.loadingPanel.Show();
        
    }
}

public enum TextTypes
{
    BEST_TXT
}